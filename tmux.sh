#!/bin/bash
# Starts new-session in -d(detached mode)
# with -s(gives name to the session)
tmux new-session -d -s "ansible"

# renames window to host
tmux rename-window -t "1" "host"

# 1st line creates and names new windows
# 2nd line ssh eval for password caching and logs in to the server
# ssha is an alias for eval (ssh-agent -c)&&ssh-add for fish shell
# to find ipaddress of the ssh machine use hostname -I
tmux new-window -n "bunty"
tmux send-keys -t bunty "ssha && ssh bunty@ipaddress" Enter
tmux new-window -n "anna"
tmux send-keys -t anna "ssha && ssh anna@ipaddress" Enter
tmux new-window -n "boobs"
tmux send-keys -t boobs "ssha && ssh boobs@ipaddress" Enter
tmux new-window -n "shinchan"
tmux send-keys -t shinchan "ssha && ssh shinchan@ipaddress" Enter

# Finally attaches to the session
tmux attach-session -d -t "ansible"
