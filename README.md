# Tmux Ansible Automation Script

## Tmux install is necessary. 

### Wrote this script with 4 VM's in mind but change can be made to add or remove VM's

# Steps

### 1. Get user and ip address using this command(works on most distro) and add it to the script

#### Example how to ssh into server:
```
ssh yobo@192.168.259.123
```

#### Get ipaddress with:
```
hostaname -I
```

#### Get User with:
```
whoami
```
### It is a bash script so either make it executable using:

```
chmod +x tmux.sh
./tmux.sh
```

### OR

### Just run it with bash:
```
bash tmux.sh
```

## ! Important
## The script uses alias for eval (ssh-agent -c)&& ssh-add as ssha

## so you can alias it in you shell or just don't add a password to the key